Expliquer un peu les différents addon et leurs utilisations

### addons supplémentaires interressants :

- [addon-console](https://github.com/storybooks/storybook-addon-console)
- [addon-events](https://github.com/storybooks/storybook/tree/release/3.4/addons/events)
- [addon-jest](https://github.com/storybooks/storybook/tree/release/3.4/addons/jest)
- or [storybook-addon-specifications](https://github.com/mthuret/storybook-addon-specifications)
