module.exports = (baseConfig, env, defaultConfig) => ({
  ...defaultConfig,
  module: {
    ...defaultConfig.module,
    rules: [
      ...defaultConfig.module.rules,
      {
        test: /\.stories\.jsx?$/,
        loaders: [
          {
            loader: require.resolve("@storybook/addon-storysource/loader"),
            options: {
              prettierConfig: {
                printWidth: 80,
                tabWidth: 2,
                bracketSpacing: true,
                trailingComma: "es5",
                singleQuote: true
              },
              uglyCommentsRegex: [/^eslint-.*/, /^global.*/]
            }
          }
        ],
        enforce: "pre"
      }
    ]
  }
});
