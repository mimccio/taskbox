import { configure, addDecorator } from "@storybook/react";
import { setOptions } from "@storybook/addon-options";
import { setDefaults } from "@storybook/addon-info";
import { withKnobs } from "@storybook/addon-knobs/react";
import { withInfo } from "@storybook/addon-info";
import { checkA11y } from "@storybook/addon-a11y";

import "../src/index.css";

const req = require.context("../src", true, /.stories.js$/);

addDecorator(checkA11y);
addDecorator((story, context) => withInfo()(story)(context));
addDecorator(
  withKnobs,
  setOptions({
    name: "Finkey Storybook",
    url: "https://finkey.pages.gitlab.finkey.fr:8443/finkey-front",
    hierarchySeparator: /\/|\./,
    hierarchyRootSeparator: /\|/
  })
);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

// addon-info
const color = "#4D4D4D";
setDefaults({
  header: false, // Toggles display of header with component name and description
  styles: {
    infoBody: { color: color },
    // infoContent: { backgroundColor: "pink" },
    header: {
      h1: {
        color: color,
        fontSize: 20
      },
      h2: {
        color: color,
        fontSize: 16
      }
    },
    source: {
      h1: {
        color: color,
        fontSize: 20
      }
    }
  }
});
