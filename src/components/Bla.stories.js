import React from "react";
import { storiesOf } from "@storybook/react";

import Bla from "./Bla";

storiesOf("Components|Dumb/Bla", module).add("default", () => (
  <Bla text="Hello You" />
));
