import React from "react";

const Say = ({ text, onButtonClick }) => {
  return (
    <div>
      <button onClick={onButtonClick}>click me</button>
      <p>{text}</p>
    </div>
  );
};

export default Say;
