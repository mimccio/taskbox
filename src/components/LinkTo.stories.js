import React from "react";
import { storiesOf } from "@storybook/react";
import { linkTo } from "@storybook/addon-links";

storiesOf("Nav|LinkTo", module).add("Button", () => (
  <button
    style={{ backgroundColor: "orange" }}
    onClick={linkTo("Components|Button")}
  >
    Go to "Button"
  </button>
));
