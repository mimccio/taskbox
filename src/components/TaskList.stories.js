import React from "react";
import { storiesOf } from "@storybook/react";
import { linkTo } from "@storybook/addon-links";
import LinkTo from "@storybook/addon-links/react";
import { withNotes } from "@storybook/addon-notes";
import { withInfo } from "@storybook/addon-info";

import { PureTaskList } from "./TaskList";
import { task, actions } from "./Task.stories";
import notes from "./TaskList.md";

export const defaultTasks = [
  { ...task, id: "1", title: "Task 1" },
  { ...task, id: "2", title: "Task 2" },
  { ...task, id: "3", title: "Task 3" },
  { ...task, id: "4", title: "Task 4" },
  { ...task, id: "5", title: "Task 5" },
  { ...task, id: "6", title: "Task 6" }
];

export const withPinnedTasks = [
  ...defaultTasks.slice(0, 5),
  { id: "6", title: "Task 6 (pinned)", state: "TASK_PINNED" }
];

storiesOf("UI|TaskList", module)
  .addDecorator((story, context) =>
    withInfo({
      styles: {
        infoBody: { color: "orange" },
        infoContent: { color: "crimson", backgroundColor: "pink" },
        header: {
          h1: {
            color: "red"
          },
          h2: { color: "pink" },
          div: { color: "purple" }
        },
        source: {
          h1: { color: "blue" },
          h2: { color: "green" },
          div: { color: "pink" }
        }
      },
      text: "String or React Element with docs about my component" // Warning! This option's name will be likely renamed to "summary" in 3.3 release. Follow this PR #1501 for details
      // other possible options see in Global options section below
    })(story)(context)
  )
  .addDecorator(story => <div style={{ padding: "3rem" }}>{story()}</div>)
  .add(
    "default",
    withNotes(notes)(() => <PureTaskList tasks={defaultTasks} {...actions} />)
  )
  .add("withPinnedTasks", () => (
    <PureTaskList
      tasks={withPinnedTasks}
      {...actions}
      onClick={linkTo("TaskList", "default")}
    />
  ))
  .add("loading", () => <PureTaskList loading tasks={[]} {...actions} />)
  .add("empty", () => <PureTaskList tasks={[]} {...actions} />)
  .add("First", () => <LinkTo story="TaskList">Go back</LinkTo>);
