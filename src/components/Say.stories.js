import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
// import { withKnobs, object } from "@storybook/addon-knobs/react";

import Say from "./Say";

storiesOf("Components|Dumb/Say", module).add("default", () => (
  <Say onButtonClick={action("on button click")} text="Hello You" />
));
