import React from "react";
import PropTypes from "prop-types";

import "./style.css";

const Button = ({ onButtonClick, text }) => (
  <div>
    <button className="button primary" onClick={onButtonClick}>
      {text}
    </button>
  </div>
);

Button.propTypes = {
  onButtonClick: PropTypes.func.isRequired,
  /** text of the button  */
  text: PropTypes.string.isRequired
};

export default Button;
