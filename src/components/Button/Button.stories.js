import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { text } from "@storybook/addon-knobs/react";
import { withNotes } from "@storybook/addon-notes";
import LinkTo from "@storybook/addon-links/react";
import centered from "@storybook/addon-centered";

import { backgrounds } from "../../lib/storybook/params";
import Button from ".";
import README from "./README.md";

storiesOf("Components|Button", module)
  .addDecorator(story => <div style={{ padding: "3rem" }}>{story()}</div>)
  .addDecorator(backgrounds)
  .addDecorator(centered)
  .add("default", () => (
    <Button
      onButtonClick={action("onButtonClick")}
      text={text("text", "Click Me !")}
    />
  ))
  .add(
    "with notes",
    withNotes("Some notes about the component")(() => (
      <Button
        onButtonClick={action("onButtonClick")}
        text={text("text", "Click Me !")}
      />
    ))
  )
  .add(
    "with Markdown file",
    withNotes(README)(() => (
      <Button
        onButtonClick={action("onButtonClick")}
        text={text("text", "Click Me !")}
      />
    ))
  )
  .add(
    "with LinkTo",
    withNotes(README)(() => (
      <div>
        <Button
          onButtonClick={action("onButtonClick")}
          text={text("text", "Click Me !")}
        />
        <div
          style={{
            position: "absolute",
            bottom: 20,
            left: 0,
            width: "100%",
            display: "flex",
            justifyContent: "center"
          }}
        >
          <LinkTo story="default">Go to Default</LinkTo>
          <LinkTo story="with notes">Go to with notes</LinkTo>
          <LinkTo story="with Markdown file">Go to with Markdown file</LinkTo>
        </div>
      </div>
    ))
  );
