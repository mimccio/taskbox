import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { withKnobs, object } from "@storybook/addon-knobs/react";
import { withNotes } from "@storybook/addon-notes";
import { withInfo } from "@storybook/addon-info";
import { linkTo } from "@storybook/addon-links";
import LinkTo from "@storybook/addon-links/react";

import Task from "./Task";

export const task = {
  id: "1",
  title: "Test Task",
  state: "TASK_INBOX",
  updatedAt: new Date(2018, 0, 1, 9, 0)
};

export const actions = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

const longTitle = `This task's name is absurdly large. In fact, I think if I keep going I might end up with content overflow. What will happen? The star that represents a pinned task could have text overlapping. The text could cut-off abruptly when it reaches the star. I hope not`;

storiesOf("Components|Dumb/Task", module)
  .addDecorator(story => <div style={{ padding: "3rem" }}>{story()}</div>)
  .add(
    "default",
    withInfo("description")(() => {
      return (
        <Task
          task={object("task", { ...task })}
          {...actions}
          onClick={linkTo("Task", "archived")}
        />
      );
    })
  )
  .add(
    "pinned",
    withNotes("Some notes on the component")(() => (
      <Task task={{ ...task, state: "TASK_PINNED" }} {...actions} />
    ))
  )
  .add("archived", () => (
    <Task task={{ ...task, state: "TASK_ARCHIVED" }} {...actions} />
  ))
  .add("long title", () => (
    <Task task={{ ...task, title: longTitle }} {...actions} />
  ));
