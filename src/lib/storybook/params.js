import addonBackgrounds from "@storybook/addon-backgrounds";

export const backgrounds = addonBackgrounds([
  { name: "default", value: "#eeeeee", default: true },
  { name: "twitter", value: "#00aced", default: false },
  { name: "facebook", value: "#3b5998" }
]);
